#include "player.h"

#include "game_config.h"
#include "bricks.h"

using namespace game;
using namespace game_config;
using namespace _bricks;

namespace game
{
	namespace _player
	{
		Player player; 

		void initialization()
		{
			player.position = { screenWidth / 2, screenHeight * screenMeasurement };
			player.size = { screenWidth / 10 , playerHeight };
			player.life = PLAYER_MAX_LIFE;
		}

		void update()
		{
			if (!gameOver)
			{
				if (!pause)
				{
					if (player.life <= 0) 
						gameOver = true;
					else
					{
						gameOver = true;

						for (int i = 0; i < LINES_OF_BRICKS; i++)
						{
							for (int j = 0; j < BRICKS_PER_LINE; j++)
							{
								if (brick[i][j].active) gameOver = false;
							}
						}
					}
				}
			}
		}

		void draw()
		{
			DrawRectangle(static_cast<int>(player.position.x - player.size.x / 2), static_cast<int>(player.position.y - player.size.y / 2), static_cast<int>(player.size.x), static_cast<int>(player.size.y), BLACK);

			for (int i = 0; i < player.life; i++) DrawRectangle(20 + 40 * i, screenHeight - 30, 35, 10, GREEN);			
		}
	}
}