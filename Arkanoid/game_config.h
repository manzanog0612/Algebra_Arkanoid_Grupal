#ifndef GAME_CONFIG_H
#define GAME_CONFIG_H

#include "game.h"

namespace game
{
	namespace game_config
	{
		const int screenWidth = 800;
		const int screenHeight = 450;

		extern bool gameOver;
		extern bool pause;
	}
}

#endif // GAME_CONFIG_H
