#include "bricks.h"

using namespace game;

namespace game
{
	namespace _bricks
	{
		Brick brick[LINES_OF_BRICKS][BRICKS_PER_LINE] = { 0 };
		Vector2 brickSize = { 0 };

		void initialization()
		{
			int initialDownPosition = 50;

			brickSize = { (float)(GetScreenWidth() / BRICKS_PER_LINE), (float)40 };

			for (int i = 0; i < LINES_OF_BRICKS; i++)
			{
				for (int j = 0; j < BRICKS_PER_LINE; j++)
				{
					brick[i][j].position = { j * brickSize.x + brickSize.x / 2, i * brickSize.y + initialDownPosition };
					brick[i][j].active = true;
				}
			}
		}

		void update()
		{

		}

		void draw()
		{
			for (int i = 0; i < LINES_OF_BRICKS; i++)
			{
				for (int j = 0; j < BRICKS_PER_LINE; j++)
				{
					if (brick[i][j].active)
					{
						if ((i + j) % 2 == 0) DrawRectangle(static_cast<int>(brick[i][j].position.x - brickSize.x / 2), static_cast<int>(brick[i][j].position.y - brickSize.y / 2), static_cast<int>(brickSize.x), static_cast<int>(brickSize.y), GRAY);
						else DrawRectangle(static_cast<int>(brick[i][j].position.x - brickSize.x / 2), static_cast<int>(brick[i][j].position.y - brickSize.y / 2), static_cast<int>(brickSize.x), static_cast<int>(brickSize.y), DARKGRAY);
					}
				}
			}
		}
	}
}