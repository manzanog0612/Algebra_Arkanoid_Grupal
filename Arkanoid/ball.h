#ifndef BALL_H
#define BALL_H

#include "game.h"

namespace game
{
	namespace _ball
	{
		struct Ball
		{
			Vector2 position;
			Vector2 speed;
			float radius;
			bool active;
		};

		extern Ball ball;

		const float screenMeasurement = 7.f / 8.f;
		const short playerHeight = 20;
		const short minAngle = 35;
		const Vector2 initialBallSpeed = { 0, -4 };

		void initialization();

		void update();

		void draw();
	}
}

#endif // BALL_H
