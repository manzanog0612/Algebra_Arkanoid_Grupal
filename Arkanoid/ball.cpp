#include "ball.h"

#include "game_config.h"
#include "player.h"
#include "bricks.h"

using namespace game;
using namespace game_config;
using namespace _player;
using namespace _bricks;

namespace game
{
	namespace _ball
	{
		Ball ball;

        static bool checkCollisionBallPlayer()
        {
            Vector2 distance;

            distance.x = ball.position.x;
            distance.y = ball.position.y;

            if (distance.x < player.position.x - player.size.x / 2) distance.x = player.position.x - player.size.x / 2;
            if (distance.x > player.position.x + player.size.x / 2) distance.x = player.position.x + player.size.x / 2;

            if (distance.y < player.position.y - player.size.y / 2) distance.y = player.position.y - player.size.y / 2;
            if (distance.y > player.position.y + player.size.y / 2) distance.y = player.position.y + player.size.y / 2;

            return sqrtf((distance.x - ball.position.x) * (distance.x - ball.position.x) + (distance.y - ball.position.y) * (distance.y - ball.position.y)) <= ball.radius;
        }

        //----------------------------------------------------------------------------------------------

		void initialization()
		{
			ball.position = { screenWidth / 2, screenHeight * screenMeasurement - playerHeight - 10 };
			ball.speed = { 0, 0 };
			ball.radius = 7;
			ball.active = false;
		}

		void update()
		{
            if (!gameOver)
            {
                if (!pause)
                {
                    if (ball.active)
                    {
                        ball.speed.y += gravity;

                        ball.position.x += ball.speed.x;
                        ball.position.y += ball.speed.y;
                    }
                    else
                    {
                        ball.position = { player.position.x, screenHeight * 7 / 8 - 30 };
                    }

                    if (((ball.position.x + ball.radius) >= screenWidth) || ((ball.position.x - ball.radius) <= 0)) ball.speed.x *= -1;
                    else if ((ball.position.y - ball.radius) <= 0) ball.speed.y *= -1;
                    else if ((ball.position.y + ball.radius) >= screenHeight)
                    {
                        ball.speed = { 0, 0 };
                        ball.active = false;

                        player.life--;
                    }

                    if (checkCollisionBallPlayer())
                    {   
                        // IMPORTANTE: Los (* 180 / PI) son porque math.h trabaja con radianes
                        if (ball.speed.y > 0)   // Para que solo choque cuando la pelota va hacia abajo
                        {
                            std::cout << "----------------------------------------" << std::endl;

                            // ----- REBOTE NORMAL -----
                            hypotenuse = sqrtf(powf(ball.speed.y, 2) + powf(ball.speed.x, 2));                              // Se vuelve a calcular porque es afectada po la gravedad
                            std::cout << "Hipotenusa:             " << hypotenuse << std::endl;

                            initialDirectionAngle = fabsf(asinf(ball.speed.y / hypotenuse) * 180 / PI);                    // Se calcula el �ngulo con el que viene la pelota

                            std::cout << "Angulo Inicial:         " << initialDirectionAngle << std::endl;

                            // Angulo final: Depende su signo de la direcci�n de la pelota
                            if (ball.speed.x < 0)
                                finalDirectionAngle = -fabsf(initialDirectionAngle);
                            else if (ball.speed.x > 0)
                                finalDirectionAngle = fabsf(initialDirectionAngle);

                            std::cout << "Angulo Result:          " << finalDirectionAngle << std::endl;

                            ball.speed.y = -fabsf(sinf(finalDirectionAngle * PI / 180) * hypotenuse);
                            ball.speed.x = (cosf(finalDirectionAngle * PI / 180) * hypotenuse);

                            if (finalDirectionAngle < 0) ball.speed.x *= -1;
                            
                            std::cout << "Y':                     " << ball.speed.y << std::endl;
                            std::cout << "X':                     " << ball.speed.x << std::endl;

                            // ----- SUMA DE MODIFICACI�N DE BARRA -----
                            barModifier = (maxBarAngle * (ball.position.x - player.position.x)) / (player.size.x / 2);   // �ngulo que modifica S/ donde impacte

                            if (barModifier > maxBarAngle) barModifier = maxBarAngle;
                            else if (barModifier < -maxBarAngle) barModifier = -maxBarAngle;

                            std::cout << "Modificion de la Barra: " << barModifier << std::endl;

                            if (ball.position.x < player.position.x) // Golpe en posicion de la Pelota con respecto a la Barra
                            {
                                if (ball.speed.x > 0)
                                {
                                    barModifier = -barModifier;
                                }

                                finalDirectionAngle += barModifier;

                                if (finalDirectionAngle < minAngle && ball.speed.x < 0)
                                {
                                    finalDirectionAngle = -minAngle;
                                }
                            }
                            else
                            {
                                finalDirectionAngle -= barModifier;
                                if (finalDirectionAngle < minAngle && ball.speed.x > 0)
                                    finalDirectionAngle = minAngle;
                            }

                            std::cout << "Direccion Final:        " << finalDirectionAngle << std::endl;

                            ball.speed.y = -fabsf(sinf(finalDirectionAngle * PI / 180) * hypotenuse);
                            ball.speed.x = (cosf(finalDirectionAngle * PI / 180) * hypotenuse);

                            if (finalDirectionAngle < 0) ball.speed.x *= -1;

                            std::cout << "Y':                     " << ball.speed.y << std::endl;
                            std::cout << "X':                     " << ball.speed.x << std::endl;

                            pause = pause;  // CAMBIAR PAUSA
                        }
                    }

                    for (int i = 0; i < LINES_OF_BRICKS; i++)
                    {
                        for (int j = 0; j < BRICKS_PER_LINE; j++)
                        {
                            if (brick[i][j].active)
                            {
                                // Hit below
                                if (((ball.position.y - ball.radius) <= (brick[i][j].position.y + brickSize.y / 2)) &&
                                    ((ball.position.y - ball.radius) > (brick[i][j].position.y + brickSize.y / 2 + ball.speed.y)) &&
                                    ((fabsf(ball.position.x - brick[i][j].position.x)) < (brickSize.x / 2 + ball.radius * 2 / 3)) && (ball.speed.y < 0))
                                {
                                    brick[i][j].active = false;
                                    ball.speed.y *= -1;
                                }
                                // Hit above
                                else if (((ball.position.y + ball.radius) >= (brick[i][j].position.y - brickSize.y / 2)) &&
                                    ((ball.position.y + ball.radius) < (brick[i][j].position.y - brickSize.y / 2 + ball.speed.y)) &&
                                    ((fabsf(ball.position.x - brick[i][j].position.x)) < (brickSize.x / 2 + ball.radius * 2 / 3)) && (ball.speed.y > 0))
                                {
                                    brick[i][j].active = false;
                                    ball.speed.y *= -1;
                                }
                                // Hit left
                                else if (((ball.position.x + ball.radius) >= (brick[i][j].position.x - brickSize.x / 2)) &&
                                    ((ball.position.x + ball.radius) < (brick[i][j].position.x - brickSize.x / 2 + ball.speed.x)) &&
                                    ((fabsf(ball.position.y - brick[i][j].position.y)) < (brickSize.y / 2 + ball.radius * 2 / 3)) && (ball.speed.x > 0))
                                {
                                    brick[i][j].active = false;
                                    ball.speed.x *= -1;
                                }
                                // Hit right
                                else if (((ball.position.x - ball.radius) <= (brick[i][j].position.x + brickSize.x / 2)) &&
                                    ((ball.position.x - ball.radius) > (brick[i][j].position.x + brickSize.x / 2 + ball.speed.x)) &&
                                    ((fabsf(ball.position.y - brick[i][j].position.y)) < (brickSize.y / 2 + ball.radius * 2 / 3)) && (ball.speed.x < 0))
                                {
                                    brick[i][j].active = false;
                                    ball.speed.x *= -1;
                                }
                            }
                        }
                    }
                }
            }
		}

		void draw()
		{
            DrawCircleV(ball.position, ball.radius, MAROON);

            DrawLine(static_cast<int>(ball.position.x), static_cast<int>(ball.position.y), static_cast<int>(ball.position.x + ball.speed.x * 100), static_cast<int>(ball.position.y + ball.speed.y * 100), GREEN);
		}
	}
}