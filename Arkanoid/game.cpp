#include "game.h"

#include "game_config.h"
#include "ball.h"
#include "bricks.h"
#include "player.h"

using namespace game;
using namespace game_config;
using namespace _ball;
using namespace _bricks;
using namespace _player;

namespace game
{
	//global vars
	float hypotenuse;
	float barModifier = 0;
	float initialDirectionAngle = 0;
	float finalDirectionAngle = 0;

	static void initialization()
	{
		InitWindow(screenWidth, screenHeight, "sample game: arkanoid");

		SetTargetFPS(60);

		_ball::initialization();
		_bricks::initialization();
		_player::initialization();
	}

	static void input()
	{
		if (!gameOver)
		{
			if (IsKeyPressed('P') || IsKeyPressed('p')) pause = !pause;

			if (!pause)
			{
				if (IsKeyDown(KEY_LEFT)) player.position.x -= playerSpeed;
				if ((player.position.x - player.size.x / 2) <= 0) player.position.x = player.size.x / 2;
				if (IsKeyDown(KEY_RIGHT)) player.position.x += playerSpeed;
				if ((player.position.x + player.size.x / 2) >= screenWidth) player.position.x = screenWidth - player.size.x / 2;

				if (!ball.active)
				{
					if (IsKeyPressed(KEY_SPACE))
					{
						ball.active = true;
						ball.speed = initialBallSpeed;
					}
				}
			}
		}
		else
		{
			if (IsKeyPressed(KEY_ENTER))
			{
				initialization();
				gameOver = false;
			}
		}
	}

	static void update()
	{
		_player::update();
		_ball::update();
		_bricks::update();
	}

	static void draw()
	{ 
		BeginDrawing();

		ClearBackground(RAYWHITE);

		if (!gameOver)
		{
			_player::draw();
			_ball::draw();
			_bricks::draw();

			if (pause) DrawText("GAME PAUSED", screenWidth / 2 - MeasureText("GAME PAUSED", 40) / 2, screenHeight / 2 - 40, 40, MAGENTA);
		}
		else DrawText("PRESS [ENTER] TO PLAY AGAIN", GetScreenWidth() / 2 - MeasureText("PRESS [ENTER] TO PLAY AGAIN", 20) / 2, GetScreenHeight() / 2 - 50, 20, GRAY);

		EndDrawing();
	}

	void run()
	{
		initialization();

		while (!WindowShouldClose())
		{
			input();
			update();
			draw();
		}

		CloseWindow();
	}
}