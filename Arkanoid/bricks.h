#ifndef BRICKS_H
#define BRICKS_H

#include "game.h"

#define LINES_OF_BRICKS 5
#define BRICKS_PER_LINE 20

namespace game
{
	namespace _bricks
	{
		struct Brick
		{
			Vector2 position;
			bool active;
		};

		extern Brick brick[LINES_OF_BRICKS][BRICKS_PER_LINE];
		extern Vector2 brickSize;

		void initialization();

		void update();

		void draw();
	}
}

#endif // BRICKS_H