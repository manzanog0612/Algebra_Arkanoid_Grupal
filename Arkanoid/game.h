#ifndef GAME_H
#define GAME_H

#include "raylib.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <math.h>

namespace game
{
	const float maxBarAngle = 55;
	extern float hypotenuse;
	extern float barModifier;
	extern float initialDirectionAngle;
	extern float finalDirectionAngle;
	const float gravity = 0.0135f;

	void run();
}

#endif // GAME_H
