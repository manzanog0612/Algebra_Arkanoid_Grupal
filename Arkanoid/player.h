#ifndef PLAYER_H
#define PLAYER_H

#include "game.h"

#define PLAYER_MAX_LIFE 5

namespace game
{
	namespace _player
	{
		struct Player
		{
			Vector2 position;
			Vector2 size;
			int life;
		};

		extern Player player;

		const float screenMeasurement = 7.f / 8.f;
		const short playerHeight = 20;
		const short playerSpeed = 5;

		void initialization();

		void update();

		void draw();
	}
}

#endif // PLAYER_H
